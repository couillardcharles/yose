"use strict";

describe('Prime Factors Controller', function () {

    var scope, controller, resource = jasmine.createSpyObj("resource", ['get', 'query']);

    beforeEach(function () {
        angular.mock.module("primefactors.resource");
        angular.mock.module("primefactors");
    });

    beforeEach(inject(function ($controller, $rootScope) {
        scope = $rootScope.$new();
        controller = $controller;
    }));

    it("should be defined", function () {
        var primeFactors = controller("PrimeFactorsController", { $scope: scope, PrimeFactorsResource: resource });

        expect(primeFactors).toBeDefined();
    });

    describe("with a loaded controller", function () {
        var primeFactorsController, options;

        beforeEach(function () {
            resource.get.andCallFake(function(data, callback) {
                options = data;
                callback({decomposition: [2, 2]});
            });
            primeFactorsController = controller("PrimeFactorsController", { $scope: scope, PrimeFactorsResource: resource });
        });

        it("should decompose number", function() {
            scope.number = "1";

            scope.decompose();

            expect(options.number).toBe("1");
            expect(scope.result).toBe("1 = 2 x 2");
        });

        it("should not decompose a string", function() {
            scope.number = "3hello";

            scope.decompose();

            expect(scope.result).toBe("3hello is not a number");
        });

        it("should handle error", function() {
            scope.number = "1000001";
            resource.get.andCallFake(function(data, callback) {
                callback({error: "too big number (>1e6)"});
            });

            scope.decompose();

            expect(scope.result).toBe("too big number (>1e6)");
        });

        it("should decompose numbers", function() {
            scope.number = "15, hello";
            resource.query.andCallFake(function(data, callback) {
                options = data;
                callback([{number: 15, decomposition: [2, 2]}, {number: "hello"}]);
            });

            scope.decompose();

            expect(options.number.length).toBe(2);
            expect(scope.result).toBe("");
            expect(scope.results.length).toBe(2);
            expect(scope.results[0]).toBe("15 = 2 x 2");
            expect(scope.results[1]).toBe("hello is not a number");
        });

    });
});
