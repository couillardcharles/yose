"use strict";

describe('Mine Controller', function () {

    var scope, controller;

    beforeEach(function () {
        angular.mock.module("minesweeper")
    });

    beforeEach(inject(function ($controller, $rootScope) {
        scope = $rootScope.$new();
        controller = $controller;
    }));

    it("should be defined", function () {
        var mineController = controller("MineController", { $scope: scope, $window: {document: {}} });

        expect(mineController).toBeDefined();
    });

    it("should load a default grid", function () {
        controller("MineController", { $scope: scope, $window: {document: {}} });

        expect(scope.grid).toBeDefined();
        expect(scope.grid.length).toBe(8);
        expect(scope.grid[0].length).toBe(8);
    });

    describe("with a grid", function () {
        var mineController, window = { document : {}};

        beforeEach(function () {
            mineController = controller("MineController", { $scope: scope, $window: window });
        });

        it("should pass grid to scope on load", function () {
            window.document = {grid: [
                ['empty', 'empty'],
                ['empty', 'bomb']
            ]};

            scope.load();

            expect(scope.grid).toBeDefined();
            expect(scope.grid[0][0].bomb).toBeFalsy();
            expect(scope.grid[1][1].bomb).toBeTruthy();
        });

        it("should reveal a cell", function () {
            scope.grid = [
                [
                    {open: false}
                ]
            ];

            scope.reveal(0, 0);

            expect(scope.grid[0][0].open).toBeTruthy();
        });

        it("should give number of bombs around when revealing a cell", function () {
            scope.grid = [
                [
                    {},
                    {}
                ],
                [
                    {bomb: true},
                    {}
                ]
            ];

            scope.reveal(0, 0);

            expect(scope.grid[0][0].text).toBe("1");
        });

        it("should give number of bombs around when revealing a cell with height bombs around", function () {
            scope.grid = [
                [
                    {bomb: true},
                    {bomb: true},
                    {bomb: true}
                ],
                [
                    {bomb: true},
                    {},
                    {bomb: true}
                ],
                [
                    {bomb: true},
                    {bomb: true},
                    {bomb: true}
                ]
            ];

            scope.reveal(1, 1);

            expect(scope.grid[1][1].text).toBe("8");
        });

        it("should give an empty number of bombs around when revealing a cell with no bomb around", function () {
            scope.grid = [
                [
                    {}
                ]
            ];

            scope.reveal(0, 0);

            expect(scope.grid[0][0].text).toBe("");
        });

        it("should reveal cells around when cell has no bomb", function() {
            scope.grid = [
                [
                    {},
                    {},
                    {}
                ],
                [
                    {},
                    {},
                    {}
                ],
                [
                    {},
                    {},
                    {bomb: true}
                ]
            ];

            scope.reveal(0,0);

            expect(scope.grid[0][0].open).toBeTruthy();
            expect(scope.grid[2][1].text).toBe("1");
            expect(scope.grid[1][2].text).toBe("1");
            expect(scope.grid[2][2].open).toBeFalsy();
        });

        it("should reveal a cell on suspect mode", function () {
            scope.grid = [
                [
                    {bomb: true},
                    {bomb: true}
                ],
                [
                    {bomb: true},
                    {bomb: true}
                ]
            ];
            scope.suspect = true;

            scope.reveal(0, 0);

            expect(scope.grid[0][0].suspect).toBeTruthy();
            expect(scope.grid[0][0].text).toBe("");
        });
    });
});
