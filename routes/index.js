module.exports = function (app) {
    app.get('/', require("./accueil").index);
    app.get('/ping', require("./ping").index);
    app.get('/primeFactors', require("./primeFactors").index);
    app.get('/primeFactors/ui', require("./primeFactorsUi").index);
    app.get('/minesweeper', require("./minesweeper").index);
    app.get('/fire/geek', require("./fire").index);
}