var request = require('request');
var url = require('url');

exports.index = function (req, res) {
    request(req.app.locals.apiUrl + "/primeFactors" + url.parse(req.url, true).search).pipe(res);
};