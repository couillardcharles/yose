var request = require('request');
var url = require('url');

exports.index = function (req, res) {
    request(req.app.locals.apiUrl + "/fire/geek" + url.parse(req.url, true).search).pipe(res);
};