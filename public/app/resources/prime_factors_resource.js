(function() {
    "use strict";

    angular.module("primefactors.resource", ["ngResource"])
        .factory('PrimeFactorsResource', ['$resource', function($resource) {
            return $resource("/primeFactors", {}, {});
        }]);
})();
