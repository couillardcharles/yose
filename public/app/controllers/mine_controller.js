(function () {
    "use strict";

    angular.module("minesweeper", [])
        .controller('MineController', ['$scope', '$window', function ($scope, $window) {
            $scope.load = function () {
                $scope.$apply(function () {
                    $scope.grid = buildGrid($window.document.grid);
                });
            };

            $window.document.grid = randomGrid();
            $scope.grid = buildGrid($window.document.grid);

            function randomGrid() {
                var result = [];
                for (var i = 0; i < 8; i++) {
                    result.push([randomContent(), randomContent(), randomContent(), randomContent(), randomContent(), randomContent(), randomContent(), randomContent()]);
                }
                return result;
            }

            function randomContent() {
                return _.random(1, 10) > 2 ? 'empty' : 'bomb';
            }

            function buildGrid(inputGrid) {
                var result = [];
                _.each(inputGrid, function (inputLine) {
                    var line = [];
                    _.each(inputLine, function (column) {
                        line.push({bomb: (column === 'bomb'), open: false, text: 'x'});
                    });
                    result.push(line);
                });
                return result;
            }

            $scope.reveal = function (x, y) {
                var cell = $scope.grid[x][y];
                cell.open = true;
                cell.suspect = $scope.suspect;
                var bombs = bombsAround(x, y);
                cell.text = bombs > 0 ? bombs.toString() : "";
                if (cell.bomb && cell.suspect) {
                    cell.text = "";
                }
                if (bombs == 0) {
                    revealAround(x, y);
                }
            }

            function bombsAround(x, y) {
                return  bombsAt(x, y - 1)
                    + bombsAt(x, y + 1)
                    + bombsAt(x - 1, y)
                    + bombsAt(x + 1, y)
                    + bombsAt(x - 1, y - 1)
                    + bombsAt(x - 1, y + 1)
                    + bombsAt(x + 1, y + 1)
                    + bombsAt(x + 1, y - 1);
            };

            function bombsAt(ligne, colonne) {
                return isBombWithCell(cellAt(ligne, colonne)) ? 1 : 0;
            }

            function cellAt(x, y) {
                if (x < 0 || x >= $scope.grid.length) {
                    return undefined;
                }
                return $scope.grid[x][y];
            };

            function isBombWithCell(cell) {
                return cell && cell.bomb;
            }

            function revealAround(x, y) {
                revealIfPossible(x, y + 1);
                revealIfPossible(x, y - 1);
                revealIfPossible(x - 1, y);
                revealIfPossible(x + 1, y);
                revealIfPossible(x + 1, y + 1);
                revealIfPossible(x + 1, y - 1);
                revealIfPossible(x - 1, y + 1);
                revealIfPossible(x - 1, y - 1);
            }

            function revealIfPossible(x, y) {
                var cell = cellAt(x, y);
                if (cell && !cell.bomb && !cell.open) {
                    $scope.reveal(x, y);
                }
            }
        }]);
})();