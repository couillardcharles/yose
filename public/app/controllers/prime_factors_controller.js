(function () {
    "use strict";

    angular.module("primefactors", ["primefactors.resource"])
        .controller('PrimeFactorsController', ['$scope', 'PrimeFactorsResource', function ($scope, resource) {
            $scope.decompose = function() {
                if (withNumbers($scope.number)) {
                    results($scope.number.split(", "));
                } else {
                    result($scope.number);
                }
            }

            function withNumbers(string) {
                return string.indexOf(", ") != -1;
            }

            function result(number) {
                resource.get({number: number}, function(data) {
                    $scope.result = resultMessage(number, data);
                    $scope.results = [];
                });
            }

            function results(numbers) {
                resource.query({number: numbers}, function(data) {
                    $scope.result = "";
                    $scope.results = _.map(data, function(decomposition) {
                        return resultMessage(decomposition.number, decomposition);
                    });
                });
            }

            function resultMessage(number, data) {
                if (!isNumber(number)) {
                    return number + " is not a number";
                } else if (data.error) {
                    return data.error;
                }
                return number + " = " + data.decomposition.join(" x ");
            }

            function isNumber(n) {
                return !isNaN(parseFloat(n)) && isFinite(n);
            }
        }]);
})();