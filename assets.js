module.exports = function (app) {

    global.nap = require('nap');

    nap({
        assets: {
            js: {
                base: [
                    '/public/app/resources/*.js',
                    '/public/app/controllers/*.js'
                ]
            },
            css: {
                all: [
                    '/public/css/style.styl'
                ]
            }
        }
    });
}

